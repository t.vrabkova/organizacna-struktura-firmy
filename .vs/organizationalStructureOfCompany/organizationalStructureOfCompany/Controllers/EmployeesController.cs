﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using organizationalStructureOfCompany.Models;
using System.Net;

namespace organizationalStructureOfCompany.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly SqlEmployeeData _employeeData;

        public EmployeesController(SqlEmployeeData employeeData)
        {
            _employeeData = employeeData;
        }

        [HttpGet]        
        public IActionResult GetEmployees()
        {
            return Ok(_employeeData.GetEmployees());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetEmployee(int id)
        {
            var employee = _employeeData.GetEmployee(id);
            if (employee != null)
            {
                return Ok(employee);
            }
            return NotFound($"Employee with id: {id} was not found.");
        }

        [HttpPost]
        public IActionResult AddEmployee(Employee employee)
        {
            _employeeData.AddEmployee(employee);
            return Created(HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + HttpContext.Request.Path + "/" + employee.Id, employee);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteEmployee(int id)
        {
            var employee = _employeeData.GetEmployee(id);
            if (employee != null)
            {
                if (_employeeData.DeleteEmployee(employee))
                {
                    return Ok();
                }
                else
                {
                    return StatusCode((int)HttpStatusCode.Forbidden, "Employee with id " + id + " is leader.");
                }
            }
            return NotFound($"Employee with id: {id} was not found.");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult EditEmployee(int id, Employee employee)
        {
            var existingEmployee = _employeeData.GetEmployee(id);
            if (existingEmployee != null)
            {
                employee.Id = existingEmployee.Id;
                _employeeData.EditEmployee(employee);
                return Ok(employee);
            }
            return NotFound($"Employee with id: {id} was not found.");
        }
    }
}
