﻿using Microsoft.AspNetCore.Mvc;
using organizationalStructureOfCompany.Models;
using System.Net;

namespace organizationalStructureOfCompany.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrganizationalStructureController : Controller
    {
        private readonly SqlOrganizationalStructureData _organizationalStructureData;

        public OrganizationalStructureController(SqlOrganizationalStructureData organizationalStructureData)
        {
            _organizationalStructureData = organizationalStructureData;
        }

        [HttpGet]
        public IActionResult GetOrganizationalStructure()
        {
            return Ok(_organizationalStructureData.GetOrganizationalStructure());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetOrganizationalNode(int id)
        {
            var node = _organizationalStructureData.GetNode(id);
            if (node != null)
            {
                return Ok(node);
            }
            return NotFound($"Node with id: {id} was not found.");
        }

        [HttpPost]
        [Route("{id:int?}")]
        public IActionResult AddOrganizationalNode(OrganizationalNode node, int id = 0)
        {
            //Inserting child node, that is not root, where requested parent doesn't exist
            if ((id != 0) && (_organizationalStructureData.GetNode(id) == null))
            {
                return NotFound($"Node with id: {id} was not found.");
            }
            //Inserting node on level 5
            if ((id != 0) && (_organizationalStructureData.GetNode(id).HierarchyId.GetLevel() == 4))
            {
                return StatusCode((int)HttpStatusCode.Forbidden, "Maximum level of this hierarchy is 4.");
            }
            //Successful inserting
            if (_organizationalStructureData.AddNode(id, node))
            {
                return Created(HttpContext.Request.Scheme + "://" + HttpContext.Request.Host + HttpContext.Request.Path + "/" + id, node);
            }
            //Inserting node, where leader doesn't exist
            return NotFound($"Employee with id: {node.LeaderRefId} was not found.");
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteOrganizationalNode(int id)
        {
            var node = _organizationalStructureData.GetNode(id);
            if (node != null)
            {
                _organizationalStructureData.DeleteNode(node);
                return Ok();
            }
            return NotFound($"Node with id: {id} was not found.");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult EditOrganizationalNode(int id, OrganizationalNode node)
        {
            var existingNode = _organizationalStructureData.GetNode(id);
            if (existingNode != null)
            {
                node.Id = existingNode.Id;
                node.HierarchyId = existingNode.HierarchyId;
                if (_organizationalStructureData.EditNode(node) != null)
                {
                    return Ok(node);
                }
                else
                {
                    return NotFound($"Employee with id: {node.LeaderRefId} was not found.");
                }
            }
            return NotFound($"Node with id: {id} was not found.");
        }
    }
}
