﻿using Microsoft.EntityFrameworkCore;

namespace organizationalStructureOfCompany.Models
{
    public class OrganizationContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<OrganizationalNode> OrganizationalStructure { get; set; }

        public OrganizationContext(DbContextOptions<OrganizationContext> options) : base(options)
        {
        }
    }
}
