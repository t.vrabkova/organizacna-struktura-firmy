﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace organizationalStructureOfCompany.Models
{

    public class SqlOrganizationalStructureData
    {
        private readonly OrganizationContext _organizationContext;

        public SqlOrganizationalStructureData(OrganizationContext organizationContext)
        {
            _organizationContext = organizationContext;
        }

        public bool AddNode(int id, OrganizationalNode node)
        {
            var employee = _organizationContext.Employees.Find(node.LeaderRefId);
            if (employee != null)
            {

                //If inserted node isn't root
                if (GetOrganizationalStructure().Count > 0)
                {
                    //Finding last child of parent of inserted node
                    HierarchyId child;
                    if (id != 0)
                    {
                        child = _organizationContext.OrganizationalStructure.FromSqlRaw("SELECT TOP 1 * FROM OrganizationalStructure WHERE HierarchyId.IsDescendantOf({0}) = 1 ORDER BY HierarchyId DESC", GetNode(id).HierarchyId).ToList().First().HierarchyId;
                        //If parent doesn't have children
                        if (child.GetLevel() == GetNode(id).HierarchyId.GetLevel())
                        {
                            child = null;
                        }
                        node.HierarchyId = GetNode(id).HierarchyId.GetDescendant(child, null);
                    }
                    else
                    {
                        child = _organizationContext.OrganizationalStructure.FromSqlRaw("SELECT TOP 1 * FROM OrganizationalStructure WHERE HierarchyId.IsDescendantOf({0}) = 1 ORDER BY HierarchyId DESC", HierarchyId.Parse("/")).ToList().First().HierarchyId;
                        node.HierarchyId = HierarchyId.Parse("/").GetDescendant(child, null);
                    }                    
                }
                else
                {
                    node.HierarchyId = HierarchyId.Parse("/1/");
                }
                node.Leader = employee;
                _organizationContext.OrganizationalStructure.Add(node);
                _organizationContext.SaveChanges();
                return true;
            }
            return false;
        }

        public void DeleteNode(OrganizationalNode node)
        {
            //Deleting node with all his child nodes
            List<OrganizationalNode> list = _organizationContext.OrganizationalStructure.FromSqlRaw("SELECT * FROM OrganizationalStructure WHERE HierarchyId.IsDescendantOf({0}) = 1", node.HierarchyId).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                _organizationContext.OrganizationalStructure.Remove(list[i]);
            }
            _organizationContext.SaveChanges();
        }

        public OrganizationalNode EditNode(OrganizationalNode node)
        {
            var existingNode = GetNode(node.Id);
            if (existingNode != null)
            {
                if (_organizationContext.Employees.Find(node.LeaderRefId) == null)
                {
                    return null;
                }
                existingNode.Name = node.Name;
                existingNode.Code = node.Code;
                existingNode.LeaderRefId = node.LeaderRefId;
                _organizationContext.OrganizationalStructure.Update(existingNode);
                _organizationContext.SaveChanges();
            }
            node.Leader = _organizationContext.Employees.Find(node.LeaderRefId);
            return node;
        }

        public OrganizationalNode GetNode(int id)
        {
            OrganizationalNode node = _organizationContext.OrganizationalStructure.Find(id);
            if (node != null)
            {
                node.Leader = _organizationContext.Employees.Find(node.LeaderRefId);
            }
            return node;
        }

        public List<OrganizationalNode> GetOrganizationalStructure()
        {
            var structure = _organizationContext.OrganizationalStructure.ToList();
            foreach (var node in structure)
            {
                node.Leader = _organizationContext.Employees.Find(node.LeaderRefId);
            }
            return structure;
        }
    }
}
