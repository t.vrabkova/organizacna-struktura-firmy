﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace organizationalStructureOfCompany.Models
{
    public class SqlEmployeeData
    {
        private readonly OrganizationContext _organizationContext;
        public SqlEmployeeData(OrganizationContext organizationContext)
        {
            _organizationContext = organizationContext;
        }

        public void AddEmployee(Employee employee)
        {
            _organizationContext.Employees.Add(employee);
            _organizationContext.SaveChanges();
        }

        public bool DeleteEmployee(Employee employee)
        {
            //If employee is leader in some organizational node
            if (_organizationContext.OrganizationalStructure.FromSqlRaw("SELECT * FROM OrganizationalStructure WHERE LeaderRefId = {0}", employee.Id).Count() > 0)
            {
                return false;
            }
            else
            {
                _organizationContext.Employees.Remove(employee);
                _organizationContext.SaveChanges();
                return true;
            }
        }

        public void EditEmployee(Employee employee)
        {
            var existingEmployee = this.GetEmployee(employee.Id);
            existingEmployee.Name = employee.Name;
            existingEmployee.Surname = employee.Surname;
            existingEmployee.Degree = employee.Degree;
            existingEmployee.Email = employee.Email;
            existingEmployee.PhoneNumber = employee.PhoneNumber;
            _organizationContext.Employees.Update(existingEmployee);
            _organizationContext.SaveChanges();
        }

        public Employee GetEmployee(int id)
        {            
            return _organizationContext.Employees.Find(id);
        }

        public List<Employee> GetEmployees()
        {
            return _organizationContext.Employees.ToList();
        }
    }
}
