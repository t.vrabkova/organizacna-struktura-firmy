﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace organizationalStructureOfCompany.Models
{
    [Table("OrganizationalStructure")]
    public class OrganizationalNode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(30)]
        [Required]
        public string Name { get; set; }

        [MaxLength(15)]
        [Required]
        public string Code { get; set; }

        [Required]
        public int LeaderRefId { get; set; }

        [ForeignKey("LeaderRefId")]
        public Employee Leader { get; set; }

        [JsonConverter(typeof(HierarchyIdConverter))]
        public HierarchyId HierarchyId { get; set; }
    }

    public class HierarchyIdConverter : JsonConverter<HierarchyId>
    {
        public override HierarchyId Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options) =>
                HierarchyId.Parse(reader.GetString());

        public override void Write(
            Utf8JsonWriter writer,
            HierarchyId id,
            JsonSerializerOptions options) =>
                writer.WriteStringValue(id.ToString());
    }
}
